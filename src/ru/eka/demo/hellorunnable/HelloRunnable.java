package ru.eka.demo.hellorunnable;

/**
 * реализация интерфейса Runnable
 *
 * Интерфейс Runnable определяется одним методом run,
 * педназначенный для размещения кода, используемого в потоке
 * Runnable-объект пересылает в конструктор Thread
 * и с помощью метода start поток запускается
 *
 */

public class HelloRunnable implements Runnable {
    public void run(){
        try {
            Thread.sleep(3000);
            System.out.println("hello from a thread!");
        } catch (InterruptedException e) {
            System.out.println("поток завершен");
        }
    }

    public static void main(String[] args) {
        (new Thread(new HelloRunnable())).start();
        try {
            Thread.sleep(2500);
            System.out.println("hello!");
        } catch (InterruptedException e) {
            System.out.println("поток завершен");
        }
    }
}
