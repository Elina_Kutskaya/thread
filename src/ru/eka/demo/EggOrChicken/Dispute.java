package ru.eka.demo.EggOrChicken;

/**
 * Класс запуска спора между 2 потоками
 *
 * @author Куцкая Э.А., 15ИТ18
 */
public class Dispute {
    public static void main(String[] args) {
        EggVoice eggVoice = new EggVoice();
        ChickenVoice chickenVoice = new ChickenVoice();
        System.out.println("Что появилось раньше? ");
        eggVoice.start();
        chickenVoice.start();
    }
}

