package ru.eka.demo.EggOrChicken;

/**
 * Класс потока "яйцо"
 *
 * @author Куцкая Э.А., 15ИТ18
 */
public class EggVoice extends Thread {

    @Override
    public void run() {
        for (int i = 0; i < 8; i++) {
            try {
                sleep(2000);
            } catch (InterruptedException e) {
            }
            System.out.println("Яйцо!");
        }
    }
}
