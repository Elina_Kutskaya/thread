package ru.eka.demo.jointUse;

/**
 * Класс для запуска
 *
 * @author Куцкая Э.А., 15ИТ18
 */
public class Main {
    public static void main(String[] args){
        new JointUse().start();
        new JointUse().start();
    }
}
