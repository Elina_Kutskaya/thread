package ru.eka.demo.jointUse;

/**
 * Класс реализующий синхронизацию ресурсов
 *
 * @author Куцкая Э.А., 15ИТ18
 */
public class JointUse extends Thread {
     volatile static int i = 0;

    @Override
    public void run() {
        for (int n = 0; n < 50; n++) {
            increment();
            System.out.println(i);
        }
    }
    public synchronized void increment() {
        ++i;
    }
}
