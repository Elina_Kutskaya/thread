package ru.eka.demo.picture;

/**
 * Класс запуска
 *
 * @author Куцкая Э.А., 15ИТ18
 */
public class Main {
    private static final String IN_FILE = "src\\ru\\eka\\demo\\picture\\inFile.txt";
    private static final String OUT_FILE = "src\\ru\\eka\\demo\\picture\\outFile.txt";
    private static final String PATH_TO_MUSIC = "src\\ru\\eka\\demo\\picture\\pic\\picture";

    public static void main(String[] args) throws InterruptedException {
        final long before = System.currentTimeMillis();
       Downloader music = new Downloader(IN_FILE,OUT_FILE,PATH_TO_MUSIC);
        music.start();
        music.join();
        final long after = System.currentTimeMillis();
        System.out.println("Завершено!");
        System.out.println("Время: " + (after-before) / 1_000);
    }
}

