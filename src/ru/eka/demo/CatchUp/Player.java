package ru.eka.demo.CatchUp;

/**
 * Класс потока с изменением приоритетов
 *
 * @author Куцкая Э.А., 15ИТ18
 */

public class Player extends Thread {
    private String name;
    private int onePriority;
    private int twoPriority;

    /**
     * Конуструктор
     *
     * @param name        имя игрока
     * @param onePriority первоначальный приоритет
     * @param twoPriority вторичный приоритет
     */
    public Player(String name, int onePriority, int twoPriority) {
        this.name = name;
        this.onePriority = onePriority;
        this.twoPriority = twoPriority;
    }

    /**
     * Конструктор по умолчанию
     */
    public Player() {
        this("Player", 10, 1);
    }

    public void run() {
        setPriority(onePriority);
        for (int i = 0; i < 100; i++) {
            try {
                sleep(1);
            } catch (InterruptedException e) {
            }
            System.out.println(name + " " + i);
            if (i == 50) {
                setPriority(twoPriority);
            }
        }
    }
}
