package ru.eka.demo.CatchUp;

/**
 * Класс для запуска догонялок
 *
 * @author Куцкая Э.А., 15ИТ18
 */

public class CatchUp {
    public static void main(String[] args) {
        Player hare = new Player("Заяц", 10, 1);
        Player turtle = new Player("Черепаха", 1, 10);
        turtle.start();
        hare.start();
    }
}
