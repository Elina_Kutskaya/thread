package ru.eka.demo.threadReadData;

/**
 * Класс для запуска программы
 * "Многопоточное считывание данных"
 *
 * @author Куцкая Э.А., 15ИТ18
 */
public class Main extends Thread {
    private static final String READ_ONE = "src/ru/eka/demo/threadReadData/ReadFileTwo.dat";
    private static final String READ_TWO = "src/ru/eka/demo/threadReadData/ReadFileOne.dat";

    public static void main(String[] args) {
        Thread threadOne = new ReadAndWriteData(READ_ONE);
        Thread threadTwo = new ReadAndWriteData(READ_TWO);
        threadOne.start();
        threadTwo.start();
    }
}
