package ru.eka.demo.threadReadData;

import java.io.*;

/**
 * Класс чтения и записи данных
 *
 * @author Куцкая Э.А., 15ИТ18
 */
public class ReadAndWriteData extends Thread {
    private static final String WRITE = "src/ru/eka/demo/threadReadData/WriterFile.dat";
    private String READING;

    ReadAndWriteData(String READING) {
        this.READING = READING;
    }

    /**
     * Метод чтения данных
     */
    @Override
    public void run() {
        try (BufferedReader bufferedReader = new BufferedReader(new FileReader(READING))) {
            final long before = System.currentTimeMillis();
            String string;
            while ((string = bufferedReader.readLine()) != null) {
                writeData(string);
                yield();
            }
            final long after = System.currentTimeMillis();
            System.out.println("Поток: " + getName() + " работал - " + (after - before));
        } catch (IOException e) {
        }
    }

    /**
     * Метод записи данных
     *
     * @param string
     */
    static synchronized void writeData(String string) {
        try (BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(WRITE, true))) {
            bufferedWriter.write(string + "\n");
        } catch (IOException e) {
        }
    }
}


