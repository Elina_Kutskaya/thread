package ru.eka.demo.music;

/**
 * Класс запуска
 *
 * @author Куцкая Э.А., 15ИТ18
 */
public class Main {
    private static final String IN_FILE_TXT = "src\\ru\\eka\\demo\\music\\inFile.txt";
    private static final String OUT_FILE_TXT = "src\\ru\\eka\\demo\\music\\outFile.txt";
    private static final String PATH_TO_MUSIC = "src\\ru\\eka\\demo\\music\\mus\\audio";

    public static void main(String[] args) throws InterruptedException {
        final long before = System.currentTimeMillis();
        Downloader music = new Downloader (IN_FILE_TXT,OUT_FILE_TXT,PATH_TO_MUSIC);
        music.start();
        music.join();
        final long after = System.currentTimeMillis();
        System.out.println("Завершено!");
        System.out.println("Время: " + (after-before) / 1_000);
    }
}

