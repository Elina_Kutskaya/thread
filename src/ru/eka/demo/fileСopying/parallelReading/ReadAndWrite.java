package ru.eka.demo.fileСopying.parallelReading;

import java.io.*;

/**
 * Класс чтения и записи файлов
 */
public class ReadAndWrite extends Thread {
    private String read;
    private String record;

    public ReadAndWrite(String read, String record) {
        this.read = read;
        this.record = record;
    }

    /**
     * Метод чтения данных
     */
    @Override
    public void run() {
        try (BufferedReader bufferedReader = new BufferedReader(new FileReader(read))) {
            final long before = System.currentTimeMillis();
            String string;
            while ((string = bufferedReader.readLine()) != null) {
                writeData(string, record);
                yield();
            }
            final long after = System.currentTimeMillis();
            System.out.println("Поток: " + getName() + " работал - " + (after - before));
        } catch (IOException e) {
        }
    }

    /**
     * Метод записи данных
     *
     * @param string
     * @param record
     */
    static synchronized void writeData(String string, String record) {
        try (BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(record, true))) {
            bufferedWriter.write(string + "\n");
        } catch (IOException e) {
        }
    }
}

