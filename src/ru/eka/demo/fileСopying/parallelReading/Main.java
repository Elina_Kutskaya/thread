package ru.eka.demo.fileСopying.parallelReading;

/**
 * Класс для запуска программы "Копирование файлов"
 * с параллельным копированием
 *
 * @author Куцкая Э.А., 15ИТ18
 */
public class Main extends Thread {
    private static final String read = "src/ru/eka/demo/fileСopying/ReadFile.dat";
    private static final String WRITE_ONE = "src/ru/eka/demo/fileСopying/parallelReading/WriteFileOne.dat";
    private static final String WRITE_TWO = "src/ru/eka/demo/fileСopying/parallelReading/WriteFileTwo.dat";

    public static void main(String[] args) {
        final long before = System.currentTimeMillis();
        try {
            Thread threadOne = new ReadAndWrite(read, WRITE_ONE);
            Thread threadTwo = new ReadAndWrite(read, WRITE_TWO);
            threadOne.start();
            threadTwo.start();
            threadOne.join();
            threadTwo.join();
        } catch (InterruptedException e) {

        }
        final long after = System.currentTimeMillis();
        System.out.println("Время работы " + (after - before));
    }
}