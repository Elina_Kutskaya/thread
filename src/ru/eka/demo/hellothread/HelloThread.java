package ru.eka.demo.hellothread;

/**
 * Создание подкласса Thread
 * <p>
 * Класс Thread сам реализует интерфейс Runnable,
 * хотя его метод run() ничего не делает.
 * подкласс класса Thread может
 * обесечить собственную реализацию метода run()
 */

public class HelloThread extends Thread {
    public void run() {
        for (int i = 10; i == 0; i--) {
            System.out.println(i + "  Hello from a thread!");
        }
    }

    public static void main(String[] args) {
        (new HelloThread()).start();
        for (int j = 0; j == 10; j++) {
            System.out.println(j + "  Hello from a thread!");
        }
    }
}
