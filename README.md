### 1) Проект "Яйцо или курица", разрешает спор: "Что появилось сначала - яйцо или курица?". ###
[Ссылка на проект](https://bitbucket.org/Elina_Kutskaya/thread/src/819ec5fdd0fdbc0ea20167767ce9dd4f9b9e1b83/src/ru/eka/demo/EggOrChicken/?at=master)

### 2) Проект "Догонялки", демонстрирует динамическое изменение приоритетов двух потоков  ###
[Ссылка на проект](https://bitbucket.org/Elina_Kutskaya/thread/src/bfa7f3299bf266c59a206fdb5fb4d51eccad550c/src/ru/eka/demo/CatchUp/?at=master)

### 3) Проект "Совместное использование ресурсов", демонстрирует два потока, выполняющих инкремент переменной i. ###
[Ссылка на проект](https://bitbucket.org/Elina_Kutskaya/thread/src/5c2af029e664df38989a9f3a20afb762bcf47109/src/ru/eka/demo/jointUse/?at=master)

### 4) Проект "Многопоточное считывание данных из файлов", демонстрирует считывание двумя потоками данных из двух разных файлов и затем запись этих данных в результирующий файл  ###
[Ссылка на проект](https://bitbucket.org/Elina_Kutskaya/thread/src/2e26d9bb55d44a48a41c062861b2274ae4ce15a9/src/ru/eka/demo/threadReadData/?at=master)

### 5) Проект "Копирование файлов", демонстрирует копирование текстового файла с заданным именем в текстовый файл по указанному пути. ###
[Ссылка на проект](https://bitbucket.org/Elina_Kutskaya/thread/src/5c2af029e664df38989a9f3a20afb762bcf47109/src/ru/eka/demo/fileСopying/?at=master)

[- параллельное копирование](https://bitbucket.org/Elina_Kutskaya/thread/src/ac7960be4a54a2fb0327f0cbd414e5894457e9a5/src/ru/eka/demo/fileСopying/parallelReading/?at=master) 

[- последовательное копирование](https://bitbucket.org/Elina_Kutskaya/thread/src/ac7960be4a54a2fb0327f0cbd414e5894457e9a5/src/ru/eka/demo/fileСopying/sequentialRead/?at=master) 

### 6) Проект "Качаем музыку", демонстрирует поиск и скачивание музыки с сайта ###
[Ссылка на проект](https://bitbucket.org/Elina_Kutskaya/thread/src/ac7960be4a54a2fb0327f0cbd414e5894457e9a5/src/ru/eka/demo/music/?at=master)

### 7) Проект "Качаем картинки", демонстрирует поиск и скачивание картинок с сайта ###
[Ссылка на проект](https://bitbucket.org/Elina_Kutskaya/thread/src/ac7960be4a54a2fb0327f0cbd414e5894457e9a5/src/ru/eka/demo/picture/?at=master)

### Проекты, демонстрирующие работу методов класса Thread ###
[Ипользования методов класса Thread в главном потоке программы](https://bitbucket.org/Elina_Kutskaya/thread/src/ac7960be4a54a2fb0327f0cbd414e5894457e9a5/src/ru/eka/demo/currenttread/?at=master)

[Реализация интерфейса Runnable](https://bitbucket.org/Elina_Kutskaya/thread/src/ac7960be4a54a2fb0327f0cbd414e5894457e9a5/src/ru/eka/demo/hellorunnable/?at=master)

[Создание подкласса Thread](https://bitbucket.org/Elina_Kutskaya/thread/src/ac7960be4a54a2fb0327f0cbd414e5894457e9a5/src/ru/eka/demo/hellothread/?at=master)

[Пример сравнивания перемещения потоков](https://bitbucket.org/Elina_Kutskaya/thread/src/ac7960be4a54a2fb0327f0cbd414e5894457e9a5/src/ru/eka/demo/interferens/?at=master)

[Запуск нескольких потоков](https://bitbucket.org/Elina_Kutskaya/thread/src/ac7960be4a54a2fb0327f0cbd414e5894457e9a5/src/ru/eka/demo/runexampl/?at=master)